#include "plateauDeJeu.h"
#include <queue>
#include <cassert>
#include "trajet.h"

PlateauDeJeu::PlateauDeJeu(int nbrLignes, int nbrColonnes) 
: _grille(nbrLignes, nbrColonnes){}

PlateauDeJeu::PlateauDeJeu(const PlateauDeJeu& source) 
: _coordonneesPorte(source._coordonneesPorte),
  _grille(source._grille)
{
    for(int i = 0; i < NBR_TRESORS; ++i) {
        _coordonneesTresors[i] = source._coordonneesTresors[i];
    }
}

PlateauDeJeu::~PlateauDeJeu(void) {}


void PlateauDeJeu::afficherGrille(void) const 
{
    std::cout << _grille;
}


inline bool PlateauDeJeu::estDeplacementOrthogonal(
                                    const std::pair<int,int>& position1, 
                                    const std::pair<int,int>& position2)
{
    return position1.first == position2.first 
        || position1.second == position2.second;
}

float PlateauDeJeu::calculerCoutDeplacementVoisin(
                                    const std::pair<int,int>& positionDepart, 
                                    const std::pair<int,int>& positionArrive) const 
{
    assert(_grille.estPositionDansLaMatrice(positionDepart.first, 
                                            positionDepart.second));
    assert(_grille.estPositionDansLaMatrice(positionArrive.first, 
                                            positionArrive.second));
    float cout = COUT_INFINI;
    const Cellule& depart = 
        _grille[positionDepart.second][positionDepart.first]; 
    const Cellule& arrive = 
        _grille[positionArrive.second][positionArrive.first];
    int denivellation = arrive.elevation - depart.elevation;

    if(positionDepart == positionArrive) cout = 0.0;
    else if((denivellation < 0 ? -denivellation: denivellation) < 2) {
        if(arrive.typeTerrain == FORET) {
            cout = (estDeplacementOrthogonal(positionDepart, positionArrive))
                   ? 2.0: 2.8;
        } else if(depart.typeTerrain == ROUTE && arrive.typeTerrain == ROUTE){
            cout = (estDeplacementOrthogonal(positionDepart, positionArrive))
                   ? 0.5 : 0.7; 
        } else if(arrive.typeTerrain != EAU) {
            cout = (estDeplacementOrthogonal(positionDepart, positionArrive))
                   ? 1.0 : 1.4; 
        }
        if(denivellation == 1) cout *= 2;
    }
    return cout;
}

void PlateauDeJeu::afficherTrajetsOptimauxCollecteTresors(void) const {
    Matrice2D<float>* matriceCoutsMinimaux;
    std::vector<std::pair<int,int>> pointsDinteret;

    pointsDinteret.push_back(_coordonneesPorte);
    for(int i = 0; i < NBR_TRESORS; ++i)
        pointsDinteret.push_back(_coordonneesTresors[i]);

    matriceCoutsMinimaux = calculerCoutsDeplacementMinimaux(pointsDinteret); 
    std::vector<std::string> nomNoeud ({"Porte", "T1", "T2", "T3"});
    std::vector<Trajet>* circuitsOptimaux 
        = determinerCircuitsOptimaux(nomNoeud, *matriceCoutsMinimaux);

    for(Trajet& circuit : *circuitsOptimaux)
        std::cout << circuit << std::endl;

    delete circuitsOptimaux;
    delete matriceCoutsMinimaux;
}

Matrice2D<float>* PlateauDeJeu::calculerCoutsDeplacementMinimaux(
                const std::vector<std::pair<int,int>>& pointsDinteret) const
{
    Matrice2D<float>* ptr_coutsTrajetMinimaux 
        = new Matrice2D<float>(pointsDinteret.size());
    int nbrLignesGrille = _grille.nbrLignes(); 
    int nbrColonnesGrille = _grille.nbrColonnes();

    Matrice2D<float> couts(nbrLignesGrille, 
                           nbrColonnesGrille);
    for(unsigned int i = 0; i < pointsDinteret.size() ; ++i)
    {
        const std::pair<int,int>& coordDepart = pointsDinteret[i];
        appliquerDijkstra(coordDepart, couts, pointsDinteret);
        for(unsigned int j = 0; j < pointsDinteret.size(); ++j) 
        {
            const std::pair<int,int>& coordArrive = pointsDinteret[j];
            (*ptr_coutsTrajetMinimaux)[i][j] = 
                couts[coordArrive.second][coordArrive.first];
        }
    }
    return ptr_coutsTrajetMinimaux;
}


void PlateauDeJeu::appliquerDijkstra(
                const std::pair<int,int>& coordDepart,
                Matrice2D<float>& couts,
                const std::vector<std::pair<int,int>>& pointsDinteret) const 
{
    int nbrLignesGrille = _grille.nbrLignes(); 
    int nbrColonnesGrille = _grille.nbrColonnes();

    assert(nbrLignesGrille == couts.nbrLignes() 
        && nbrColonnesGrille == couts.nbrColonnes());

    std::set<std::pair<int,int>> destinationsAVisiter(pointsDinteret.begin(), 
                                                      pointsDinteret.end());

    bool neDoitPasVisiterToutLePlateau = !destinationsAVisiter.empty(); 

    std::priority_queue<
                    std::pair<float, std::pair<int,int>>, 
                    std::vector<std::pair<float, std::pair<int,int>>>, 
                    std::greater<std::pair<float, std::pair<int,int>>>> pq;
    Matrice2D<bool> ontEteVisites (nbrLignesGrille, nbrColonnesGrille);
    ontEteVisites.remplir(false);
    couts.remplir(COUT_INFINI); 
    couts[coordDepart.second][coordDepart.first] = 0.0;
    pq.push(make_pair(0.0, coordDepart));
    while(!pq.empty()) 
    {
        const std::pair<int,int> coordAVisiter = pq.top().second;
        pq.pop();
        ontEteVisites[coordAVisiter.second][coordAVisiter.first] = true;
        if(neDoitPasVisiterToutLePlateau) {
            destinationsAVisiter.erase(coordAVisiter); 
            if(destinationsAVisiter.empty()) break;
        }

        for(int y = coordAVisiter.second - 1; y <= coordAVisiter.second + 1; ++y) 
        {
            if(y < 0 || y >= nbrLignesGrille) continue;
            for(int x = coordAVisiter.first - 1; x <= coordAVisiter.first + 1; ++x) 
            {
               int xi;
               if(x < 0 || x >= nbrColonnesGrille) 
                   xi = (x<0? nbrColonnesGrille-1 : 0);
               else xi = x;
               if(!ontEteVisites[y][xi]) 
               {
                   float coutDeplacementCourant = 
                         calculerCoutDeplacementVoisin(coordAVisiter, 
                                               std::make_pair(xi,y));
                   float coutDeplacementTotal = 
                    couts[coordAVisiter.second][coordAVisiter.first] 
                    + coutDeplacementCourant; 
                   if(coutDeplacementTotal < couts[y][xi]) 
                   {
                        couts[y][xi] = coutDeplacementTotal;
                        pq.push(std::make_pair(coutDeplacementTotal, 
                                               std::make_pair(xi,y)));
                   }
                   
               }
            }
        }
    }
}


std::ostream& operator << (std::ostream& os, const PlateauDeJeu::Cellule& c) 
{
    os << char(c.typeTerrain) << c.elevation;
    return os;
}

std::ostream& operator << (std::ostream& os, const PlateauDeJeu& plateau) 
{
    os << plateau._grille 
    << "\nCoordonnees de la porte: " << plateau._coordonneesPorte 
    << "\nCoordonnees des tresors:\n";
    for(int i = 0; i < PlateauDeJeu::NBR_TRESORS; ++i) {
        os << "t" << i+1 << ": " << plateau._coordonneesTresors[i]
           << '\n';
    }
    return os;
}


std::ostream& operator << (std::ostream& os, 
                           const std::pair<int,int>& coordonnees) 
{
    os << "(" << coordonnees.first << "," << coordonnees.second << ")";
    return os;
}

bool PlateauDeJeu::estCoordonneeDuTresorValide(
                        const std::pair<int,int>& positionDuTresor, 
                        const std::set<std::pair<int,int>>& positionsDeleau) 
{
    bool reponse = false;
    if(_grille.estPositionDansLaMatrice(positionDuTresor.first, 
                                        positionDuTresor.second)) 
    {
        reponse = (positionsDeleau.count(positionDuTresor) == 0); 
    }
    return reponse;
}

PlateauDeJeu::Cellule::Cellule(const char& typeTerrain, 
                               const int& elevation) 
{
    assert(typeTerrain == PLAINE
        || typeTerrain == EAU
        || typeTerrain == FORET
        || typeTerrain == ROUTE
        || typeTerrain == PORTE);
    this->typeTerrain = (enum terrain_t)typeTerrain;
    assert(elevation >= 1 && elevation <= 9);
    this->elevation = elevation;

}


std::istream& operator >> (std::istream& is, PlateauDeJeu& plateau) 
{
    std::set<std::pair<int,int>> coordonneesDeleau;
    bool estPortePresente = false;
    int nbrLignes(plateau._grille.nbrLignes()); 
    int nbrColonnes(plateau._grille.nbrColonnes());
    for(int y = 0; y < nbrLignes; ++y) 
    {
        std::string ligneDuPlateau;
        is >> ligneDuPlateau;
        for(int x = 0; x < nbrColonnes; ++x) 
        {
            PlateauDeJeu::Cellule caseLu(ligneDuPlateau[2*x], 
                                         ligneDuPlateau[2*x+1]-'0');
            if(caseLu.typeTerrain == PlateauDeJeu::PORTE) {
                if(estPortePresente) {
                    std::cerr << "Erreur: il ne doit y avoir " 
                              << "qu'une seule porte!\n";
                    exit(2);
                } else {
                    estPortePresente = true;
                    plateau._coordonneesPorte = std::make_pair(x,y);
                }
            } else if(caseLu.typeTerrain == PlateauDeJeu::EAU) {
                coordonneesDeleau.emplace(x,y);
            }
            plateau._grille[y][x] = caseLu;
        }
    }
    if(!estPortePresente) {
        std::cerr << "Erreur: il doit y avoir une porte!" << '\n';
        exit(3);
    }

    for(int i = 0; i < PlateauDeJeu::NBR_TRESORS; ++i) 
    {
        std::pair<int,int> coordonneesTresor;
        is >> coordonneesTresor.first >> coordonneesTresor.second;
        if(!plateau.estCoordonneeDuTresorValide(coordonneesTresor,
                                                coordonneesDeleau)) 
        {
            std::cerr << "Erreur: Les coordonnees du tresor " << i+1 << ": "
                      << coordonneesTresor << " sont invalides." << '\n';
            exit(4);
        }
        plateau._coordonneesTresors[i] = coordonneesTresor;
    }
    return is;
}

