#include <iostream>
#include <fstream>
#include "plateauDeJeu.h"

int main (int argn, char* args[]) {
    if(argn != 2) {
        std::cerr << "Usage: " << args[0] << " <nomFichier> " << '\n';
        exit(-1);
    }
    std::fstream fichier(args[1]);
    if(!fichier.is_open()) {
        std::cerr << "Erreur lors de l'ouverture du fichier " << args[1] 
                  << '\n';
        exit(-2);
    }
    int nbrLignes, nbrColonnes;
    fichier >> nbrColonnes >> nbrLignes;

    if(nbrLignes <= 1 || nbrColonnes <= 1) 
    {
        std::cerr << "Erreur: nombre de lignes et/ou de colonnes invalide" 
        << '\n';
        exit(1);
    }

    PlateauDeJeu plateau(nbrLignes, nbrColonnes);
    fichier >> plateau;
    plateau.afficherTrajetsOptimauxCollecteTresors();

    return 0;
}
