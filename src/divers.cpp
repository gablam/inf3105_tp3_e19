#include "divers.h"
#include <cmath>

using namespace std;

int comparer(float f1, float f2, float epsilon) {
    int reponse;
    float diff = f1-f2;
    if(abs(diff) < epsilon)
        reponse = 0;
    else
        reponse = (diff < 0)? -1: 1;
    return reponse;
}
