#ifndef TRAJET_H
#define TRAJET_H

/**
 * @author Gabriel Lamothe
 */

#include <vector>
#include <string>
#include <limits>
#include "matrice2D.h"


/**
 * Cette classe definie un trajet comme etant une sequence
 * de transitions d'un lieu a un autre.
 * A chaque transition est associee un cout 
 * La somme du cout de toutes les transitions donne le cout total du trajet.
 */
class Trajet {
public:
    /**
     * Constructeur par defaut.
     */
    Trajet(void) : _cout(0.0) {}

    /**
     * Accesseur du cout du trajet.
     */
    inline float cout(void) const { return _cout;}

    /**
     * Ajoute une transition(passage d'un lieu a un autre) au trajet.
     * @param depart
     *  Le nom du lieu de depart.
     * @param destination
     *  Le nom de la destination (arrive).
     * @cout
     *  Le cout pour aller du depart a la destination.
     */
    inline void ajouterTransition(const std::string& depart, 
                             const std::string& destination, 
                             float cout); 
    /**
     * Retire toutes les transitions du trajet 
     * et remet le cout de ce dernier a 0.
     */
    inline void vider(void);
private:

    friend std::ostream& operator << (std::ostream& os, 
                                      const Trajet& trajet);
    /**
     * La sequence de transitions du trajet.
     */
    std::vector<std::pair<
                std::pair<std::string,std::string>, float>> _transitions;
    /**
     * Le cout du trajet.
     */
    float _cout;
};

/**
 * Surcharge de l'operateur << pour Trajet.
 */
std::ostream& operator << (std::ostream& os, const Trajet& trajet); 

/**
 *  Determine tous les trajets optimaux(dont les couts sont minimaux) 
 *  partant et terminant au lieu (0) de la matrice de couts.
 * @param nomNoeuds
 *   Le nom des differentes destinations (noeuds du trajets).
 * @param couts
 *   La matrice de couts entre les differentes destinations par lesquelles 
 *   le trajet doit passer en excluant les destinations inaccessibles 
 *   (cout infini pour s'y rendre a partir du point de depart du trajet).
 * @cout_infini
 *   optionnel : indique la valeur utilisee dans la matrice de couts 
 *   pour representer l'infini.
 * ATTENTION : l'appelant doit liberer (delete) 
 *             le vecteur retourne par cette fonction.
 *
 * @example
 * Format de la matrice devant etre recu en parametre par la fonction. 
 *  -----------------------------------
 *  depart\arrive |  A   |  B   |  C  |
 *  ----------------------------------
 *  A             | 0.0  | 1.2  | 2.3 |
 *  B             | 2.3  | 0.0  | 3.2 |
 *  C             | 3.0  | 3.4  | 0.0 |
 *  -----------------------------------
 *  NB : La matrice recu inclus uniquement les nombres (float).
 *  Les lettres {A,B,C} representant les lieux 
 *  sont presentes uniquement a titre indicatif.
 */
std::vector<Trajet>* determinerCircuitsOptimaux(
                            const std::vector<std::string>& nomNoeuds,
                            const Matrice2D<float>& couts, 
                            const float cout_infini 
                                = std::numeric_limits<float>::infinity());
#endif
