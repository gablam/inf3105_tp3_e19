#include <cassert>

template <class T>
Matrice2D<T>::Matrice2D(int nbrLignes, int nbrColonnes) 
    : _nbrLignes(nbrLignes), _nbrColonnes(nbrColonnes) 
{
    assert(nbrLignes > 0 && nbrColonnes > 0);
    _grille = new T*[_nbrLignes];
    for(int i = 0; i < _nbrLignes; ++i) {
        _grille[i] = new T[_nbrColonnes];
    }
}


template <class T>
Matrice2D<T>::Matrice2D(const Matrice2D& source)
    : _nbrLignes(source._nbrLignes), _nbrColonnes(source._nbrColonnes)
{
    _grille = new T* [_nbrLignes];
    for(int i = 0; i < _nbrLignes; ++i) 
    {
        _grille[i] = new T[_nbrColonnes];
        for(int j = 0; j < _nbrColonnes; ++j) 
        {
            _grille[i][j] = source._grille[i][j];
        }
    }
}


template <class T>
Matrice2D<T>::~Matrice2D(void) 
{ 
    vider();
}

template<class T>
void Matrice2D<T>::remplir (const T& element) 
{
    for(int i = 0; i < _nbrLignes; ++i) {
        for(int j = 0; j < _nbrColonnes; ++j) {
            _grille[i][j] = element;
        }
    }
}

template <class T>
void Matrice2D<T>::vider(void) 
{    
    for(int i = 0; i < _nbrLignes; ++i) {
        delete[] _grille[i];
    }
    delete[] _grille;
    _grille = nullptr;
}


template<class T>
inline bool Matrice2D<T>::estPositionDansLaMatrice(int x, int y) const 
{
    return x >= 0 && x < _nbrColonnes
        && y >= 0 && y < _nbrLignes;
}


template <class T>
const T* Matrice2D<T>::operator [] (int index) const 
{
    assert(index >= 0 && index < _nbrLignes);
    return _grille[index]; 
}

template <class T>
T* Matrice2D<T>::operator [] (int index) 
{
    assert(index >= 0 && index < _nbrLignes);
    return _grille[index];
}

template <class T>
Matrice2D<T>& Matrice2D<T>::operator = (const Matrice2D<T>& source) 
{
    if(this != &source) {
        vider();
        _nbrColonnes = source._nbrColonnes;
        _nbrLignes = source._nbrLignes;

        _grille = new T* [_nbrLignes];
        for(int i = 0; i < _nbrLignes; ++i) {
            _grille[i] = new T[_nbrColonnes];
            for(int j = 0; j < _nbrColonnes; ++j) {
                _grille[i][j] = source._grille[i][j];    
            }
        }
    }
    return *this;
}


template<class T> 
std::ostream& Matrice2D<T>::grilleVersFlux(std::ostream& os) const {
    os << "-";
    for(int j = 0; j < _nbrColonnes; ++j) {
        os << "-----";
    }
    for(int i = 0; i < _nbrLignes; ++i) {
       os << "\n| ";
        for(int j = 0; j < _nbrColonnes; ++j) {
            os << _grille[i][j] << " | ";  
        }
        os << "\n-";
        for(int j = 0; j < _nbrColonnes; ++j) {
            os << "-----";
        }
    }
    return os;
}

