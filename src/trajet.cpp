#include "trajet.h"
#include "divers.h"
#include <algorithm>
#include <iomanip> 

using namespace std;

inline void Trajet::ajouterTransition(const string& depart, 
                             const string& destination, 
                             float coutDeplacement) 
{
    _transitions.push_back(make_pair(
                      make_pair(depart, destination), coutDeplacement));
    _cout += coutDeplacement;
}

inline void Trajet::vider(void) 
{
    _transitions.clear(); 
    _cout = 0.0;
}

vector<Trajet>* determinerCircuitsOptimaux(
                                const vector<string>& nomNoeuds,
                                const Matrice2D<float>& couts,
                                const float cout_infini) 
{
    assert(couts.nbrLignes() == couts.nbrColonnes());
    unsigned int nbrDestinations = couts.nbrLignes();
    assert(nomNoeuds.size() == nbrDestinations);

    vector<Trajet>* circuitsOptimaux
        = new vector<Trajet>();
    vector<int> sequence;
    
    for(unsigned int j = 0; j < nbrDestinations ; ++j) {
        if(couts[0][j] != cout_infini)
            sequence.push_back(j);
    }
    unsigned int nbrDestinationsAccessibles = sequence.size();
    sequence.push_back(0);

    float coutMinCircuit = cout_infini;
    const auto borneinfPermutation = ++sequence.begin();
    const auto bornesupPermutation = --sequence.end();
    do {
        Trajet circuitCourant;

        for(unsigned int i = 0; i < nbrDestinationsAccessibles; ++i) {
            unsigned int depart = sequence[i]; 
            unsigned int destination = sequence[i+1];
            float coutDeplacement = couts[depart][destination];
            circuitCourant.ajouterTransition(nomNoeuds[depart], 
                                             nomNoeuds[destination], 
                                             coutDeplacement);
        }
        int resultatComparaison = comparer(circuitCourant.cout(), 
                                           coutMinCircuit);
        if(resultatComparaison <= 0) 
        { 
            if(resultatComparaison < 0)
                circuitsOptimaux->clear();
            coutMinCircuit = circuitCourant.cout();
            circuitsOptimaux->push_back(circuitCourant);
        }
    
    } while(next_permutation(borneinfPermutation,
                             bornesupPermutation));
    return circuitsOptimaux;
}


ostream& operator << (ostream& os, const Trajet& trajet) 
{
    os << fixed << setprecision(1);
    for(auto& transition : trajet._transitions) 
    {
        os << transition.first.first << " -> " << transition.first.second
           << " : " << transition.second << '\n';
    }
    os << "Total : " << trajet._cout << '\n';
    os << defaultfloat;
    return os;
}

