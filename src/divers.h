#ifndef DIVERS_H
#define DIVERS_H

/**
 * @author Gabriel Lamothe
 */

/**
 *  Determine: 
 *  - si deux float sont approximativement egal (a un epsilon pres)
 *  - sinon lequel est superieur a l'autre
 * @param f1 
 *  Premier float a comparer
 * @param f2 
 *  Deuxieme float a comparer
 * @param epsilon
 *  l'incertitude toleree pour considerer 
 *  deux float identiques
 * @return 
 *  0 si : abs(f1-f2) < epsilon
 *  1 si : f1 >= f2+epsilon
 * -1 sinon
 */
int comparer(float f1, float f2, float epsilon = 0.009);


#endif
