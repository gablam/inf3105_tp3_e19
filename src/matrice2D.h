#ifndef MATRICE2D_H
#define MATRICE2D_H

/**
 *@author Gabriel Lamothe
 */

#include <iostream>

/**
 * Cette classe generique definie une matrice 2D 
 * pouvant contenir des elements d'un type quelconque. 
 */
template <class T>
class Matrice2D {

public:
    /**
     * Constructeur :
     *  @param nbrLignes
     *   Nombre de lignes de la matrice.
     *  @param nbrColonnes
     *   Nombre de colonnes de la matrice.
     *  @pre
     *   nbrLignes > 0 && nbrColonnes > 0
     */
    Matrice2D(int nbrLignes, int nbrColonnes);

    /**
     * Constructeur de matrice carre:
     *  @param dimension 
     *   La dimension de la matrice carre.
     */
    inline Matrice2D(int dimension) : Matrice2D(dimension, dimension){}; 

    /**
     * Constructeur de copie.
     */
    Matrice2D(const Matrice2D& source);

    /**
     * Destructeur.
     */
    ~Matrice2D(void);

    /**
     * Permet de remplir la matrice d'un element passe en parametre.
     * @param element
     *  L'element a inserer dans toutes les cases de la matrice.
     */
    void remplir (const T& element);

    /**
     * Vide la matrice. (libere l'espace memoire occupees par la matrice).
     */
    void vider(void);

    /**
     * Determine si la position (x,y) est dans les limites de la matrice.
     * @param x
     * La coordonnee horizontale (# de colonne)
     * @param y 
     *  la coordonnee verticale (# de ligne)
     * @return  
     *  true si la position (x,y) est dans les limites de la matrice, 
     *  false sinon.
     */
    inline bool estPositionDansLaMatrice(int x, int y) const;

    /**
     * Accesseur du nombre de lignes de la matrice.
     * @return 
     *  Le nombre de lignes de la matrice.
     */
    inline int nbrLignes(void) const { return _nbrLignes;}

    /**
     * Accesseur du nombre de colonnes de la matrice.
     * @return 
     *  Le nombre de colonnes de la matrice.
     */
    inline int nbrColonnes(void) const {return _nbrColonnes;}

    /**
     * Convertit en stream la matrice (notamment pour l'afficher).
     */
    std::ostream& grilleVersFlux(std::ostream& os) const;

    /**
     * Surchage de l'operateur [] pour acceder a une case de la matrice 
     * (en lecture seulement).
     * @example 
     *  Matrice2D<int> matrice;
     *  matrice[0][0]  // retourne le contenu de la case (0,0) de la matrice.
     */
    const T* operator [] (int index) const;

    /**
     * Surchage de l'operateur [] pour acceder a une case de la matrice 
     * (en lecture/ecriture).
     * @example 
     *  Matrice2D<int> matrice;
     *  matrice[0][0] = 2  // Accede a la case (0,0) de la matrice 
     *                     // et y place la valeur 2.
     */
    T* operator [] (int index);

    /**
     * Surchage de l'operateur d'affectation =.
     */ 
    Matrice2D& operator = (const Matrice2D& source);

private:

    /**
     * La "racine" de la strucutre contenant les elements de la matrice.
     */
    T** _grille;

    /**
     * Nombre de lignes de la matrice. 
     */
    int _nbrLignes;

    /**
     * Nombre de colonnes de la matrice. 
     */
    int _nbrColonnes;

    friend std::ostream& operator << (std::ostream& os, 
                                      const Matrice2D<T>& matrice2d) 
    {
        return matrice2d.grilleVersFlux(os);
    }
};

#include "matrice2D.hpp"
#endif
