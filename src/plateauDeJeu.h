#ifndef PLATEAUDEJEU_H
#define PLATEAUDEJEU_H

/**
 * @author Gabriel Lamothe
 */

#include <iostream>
#include <utility>
#include <set>
#include <vector>
#include <limits>
#include "matrice2D.h"

/**
 * Constante indiquant un cout de deplacement "infini"
 */
static const float& COUT_INFINI = std::numeric_limits<float>::infinity();

/**
 * Cette classe definie un plateau de jeu.
 * un Plateau de jeu contient notamment une matrice 2D (grille) de cellules, 
 * elles-memes caracterisees par deux choses : 
 *  - le type de terrain 
 *  - son elevation
 *  Cette classe permet de connaitre toutes les sequences 
 *  optimaux (cout du trajet minimal) dans lesquels aller 
 *  chercher les tresors en partant et en retournant a la porte.
 */
class PlateauDeJeu {
public:

    /**
     * Constructeur par defaut.
     * @param nbrLignes 
     *  Le nombre de lignes de la matrice du plateau de jeu.
     * @param nbrColonnes 
     *  Le nombre de colonnes de la matrice du plateau de jeu.
     */
    PlateauDeJeu(int nbrLignes = 2, int nbrColonnes = 2); 

    /**
     * Constructeur de copie.
     */
    PlateauDeJeu(const PlateauDeJeu& source);

    /**
     * Destructeur.
     */
    ~PlateauDeJeu(void);

    /**
     * Affiche la grille du plateauDeJeu.
     */
    void afficherGrille(void) const;

    /**
     * Accesseur du nombre de lignes de la grille du plateau.
     * @return 
     *  le nombre de lignes de la grille du plateau.
     */
    inline int nbrLignesGrille(void) const { return _grille.nbrLignes();}

    /**
     * Accesseur du nombre de lignes de la grille du plateau.
     * @return 
     *  le nombre de colonnes de la grille du plateau.
     */
    inline int nbrColonnesGrille(void) const { return _grille.nbrColonnes();}

    /**
     * Affiche les trajets optimaux pour la collecte des tresors 
     * en partant et revenant a la porte. 
     * NB: Les tresors inaccessibles sont ignores.
     */
    void afficherTrajetsOptimauxCollecteTresors(void) const;

private:

    /**
     * constante indiquant le nombre de tresors sur le plateau.
     */
    constexpr const static int NBR_TRESORS = 3;

    /**
     * L'ensemble des types de terrain possibles 
     * et le char lui etant associe.
     */
    enum terrain_t : char {
        PLAINE = 'N',
        EAU = 'E',
        FORET = 'F',
        ROUTE = 'R',
        PORTE = 'P'
    };

    /**
     * Structure decrivant le contenu d'une cellule de la grille du plateau.
     * contient notamment le type du terrain et l'elevation de celui-ci.
     */
    struct Cellule {
        /**
         * Constructeur par defaut.
         */
        Cellule(void) = default;

        /**
         * Constructeur de cellule: 
         * construit une cellule a partir du type de terrain 
         * et son elevation.
         * @param typeTerrain
         *  Le type du terrain de la cellule.
         * @param elevation
         *  L'elevation de la cellule.
         * @invariant 
         *  elevation >= 1 && elevation <= 9
         */
        Cellule(const char& typeTerrain, const int& elevation);

        /**
         * Le type du terrain de la cellule.
         */
        enum terrain_t typeTerrain;
        /**
         * L'elevation du terrain de la cellule.
         */
        int elevation;
    };

    /**
     * Determine si les coordonnees d'un tresor est valide.
     * @param positionDuTresor 
     *  Les coordonnees du tresor dans la grille du plateau.
     * @param positionsDeleau 
     *  L'ensemble des positions des case d'eau sur le plateau.
     * @return 
     *  true si la position du tresor est valide 
     *  false sinon.
     */
    bool estCoordonneeDuTresorValide(
        const std::pair<int,int>& positionDuTresor, 
        const std::set<std::pair<int,int>>& positionsDeleau); 
    
    /**
     * Determine si un deplacement est orthogonal ou non.
     * @param position1
     *  L'une des deux positions (depart ou arrive).
     * @param position2
     *  L'autre position (arrive ou depart).
     * @return 
     *  true si le deplacement de position1 a position2 ou inversement
     *  est orthogonal, false sinon.
     *
     */
    inline static bool estDeplacementOrthogonal(
                                    const std::pair<int,int>& position1, 
                                    const std::pair<int,int>& position2);

    /**
     * Calcul le cout d'un deplacement entre deux case voisines.
     * @param positionDepart
     *  Les coordonnees (ligne,colonne) de la case de depart.
     * @param positionArrive
     *  Les coordonnees (ligne,colonne) de la case d'arrivee.
     * @return 
     *  Le cout du deplacement.
     * @pre
     *  positionDepart et positionArrive sont dans les limites 
     *  de la matrice et sont voisines.
     */
    float calculerCoutDeplacementVoisin(
                            const std::pair<int,int>& positionDepart, 
                            const std::pair<int,int>& positionArrive) const; 

    /**
     * Genere une matrice des couts de deplacement minimaux entre
     * differents points d'interets (passe en parametre) sur le plateau .
     * @param pointsDinteret 
     *  Les coordonnees sur le plateau entre lesquels 
     *  on veut les couts minimaux de deplacement. 
     * @return 
     *  Une matrice carre de couts de dimension pointsDinteret.size().
     *  La matrice est sur le "heap". 
     * ATTENTION : l'appelant doit liberer (delete) 
     * la Matrice2D retourne par cette fonction.
     *
     * @example 
     * Exemple de matrice de couts retournees par la methode. 
     *  -----------------------------------
     *  depart\arrive |  A   |  B   |  C  |
     *  ----------------------------------
     *  A             | 0.0  | 1.2  | 2.3 |
     *  B             | 2.3  | 0.0  | 3.2 |
     *  C             | 3.0  | 3.4  | 0.0 |
     *  -----------------------------------
     *  NB : La matrice retournee inclus uniquement les nombres (float).
     *  Les lettres {A,B,C} representant les lieux 
     *  sont presentes uniquement a titre indicatif.
     */
    Matrice2D<float>* calculerCoutsDeplacementMinimaux(
                const std::vector<std::pair<int,int>>& pointsDinteret) const;

    /**
     * Applique l'algorithme de plus court chemin de Dijkstra 
     * sur le plateau de jeu.
     * @param coordDepart
     *  Les coordonnees dans le grille du plateau du point 
     *  de depart de l'algorithm.
     * @param couts
     *  La matrice des couts minimaux pour se rendre de coordDepart 
     *  a n'importe quel point sur le plateau.
     * @param pointsDinteret 
     *  Les destinations pour lesquels ont souhaite 
     *  connaitre le cout minimal pour s'y rendre a partir de coordDepart. 
     *  Si pointsDinteret est vide, ou contient une ou plusieurs coordonnees
     *  inaccessibles a partir de coordDepart, l'algorithme visite toutes les 
     *  composantes connexes a coordDepart sur le plateau. 
     *  Sinon, l'algorithme se termine lorsque tous les points 
     *  d'interets ont ete visites.
     *  @pre 
     *   la matrice de couts passee en parametre doit etre 
     *   de la meme taille (nbrLignes et nbrColonnes) que 
     *   la grille du plateau de jeu.
     *
     */
    void appliquerDijkstra(
                const std::pair<int,int>& coordDepart,
                Matrice2D<float>& couts,
                const std::vector<std::pair<int,int>>& pointsDinteret) const; 
    /**
     * Les coordonnees des differents tresors dans la grille du plateau.
     */
    std::pair<int,int> _coordonneesTresors[NBR_TRESORS];

    /**
     * Les coordonnees de la porte dans la grille du plateau.
     */
    std::pair<int,int> _coordonneesPorte;

    /**
     * La grille du plateau de jeu.
     */
    Matrice2D<Cellule> _grille;

    friend std::ostream& operator << (std::ostream& os, const PlateauDeJeu& plateau);
    friend std::istream& operator >> (std::istream& is, PlateauDeJeu& plateau);
    friend std::ostream& operator << (std::ostream& os, const PlateauDeJeu::Cellule& c);

};

/**
 * Surcharge de l'operateur << pour PlateauDeJeu::Cellule.
 */
std::ostream& operator << (std::ostream& os, const PlateauDeJeu::Cellule& c);

/**
 * Surcharge de l'operateur << pour pair<int,int>.
 */
std::ostream& operator << (std::ostream& os, 
                           const std::pair<int,int> &coordonnees);

/**
 * Surcharge de l'operateur << pour PlateauDeJeu.
 */
std::ostream& operator << (std::ostream& os, const PlateauDeJeu& plateau);

/**
 * Surcharge de l'operateur >> pour PlateauDeJeu.
 */
std::istream& operator >> (std::istream& is, PlateauDeJeu& plateau);
#endif

