export DEBUG=no
export EXEC=tp3
SRC_DIR=src
TESTS_DIR=tests
TEST_EXEC = $(patsubst %.cpp,%,$(wildcard $(SRC_DIR)/test*.cpp))

.PHONY: exec clean source test testdir testbin launchUnitaryTests

exec: source
	@cp $(SRC_DIR)/$(EXEC) ./

clean:
	@$(MAKE) clean -C $(SRC_DIR)
	@rm $(EXEC)
	@rm -rf $(TESTS_DIR)

source:
	@$(MAKE) -C $(SRC_DIR)

test: exec testbin launchUnitaryTests

testdir:
	@mkdir -p $(TESTS_DIR)

testbin: testdir source
	@$(MAKE) test -C $(SRC_DIR)
	@cp $(TEST_EXEC) $(TESTS_DIR)

launchUnitaryTests:
	@for test in `ls $(TESTS_DIR)` ; do \
		./$(TESTS_DIR)/$$test; \
	done
